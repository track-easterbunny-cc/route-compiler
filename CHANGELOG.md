# track.easterbunny.cc Route Compiler Changelog
This is the changelog for the track.easterbunny.cc Route Compiler. It details all the changes made in releases of the route compiler.

## Version 1.0.0 (track.easterbunny.cc Version 4.5.4)
* The route compiler is now open source!
* Adds in a ds_api_key variable for configuration.