# Note: repository deprecated
The Route Compiler repository has now been deprecated, as the route compiler has been wrapped into the TEBCC main repository (under the `compiler` folder).

Nonetheless, this code will remain online for anyone who wants to use it.

# track.easterbunny.cc Route Compiler

Python-based compiler to compile in data from a .tsv file for use in track.easterbunny.cc.

# Getting started
To get started, you'll need:
* A computer
* Python 3 with requests & vincenty (install by running this command: `pip3 install requests vincenty`)
* A route spreadsheet in software that can be converted to a .TSV file
* A Dark Sky API key for weather data (optional, but highly recommended)

Download this repository by using Git (`git clone https://gitlab.com/track-easterbunny-cc/route-compiler.git`), using the download button at the top of this page, or with the Releases tab. Unzip it on your computer wherever you want it to go (for git clone, you can append the directory that you want the route-compiler to be stored in. Otherwise, it'll clone the files over to `(your working directory)/route-compiler`).

# A few notes
As of v5.5.1, the route compiler is more robust and less of an internal tool (aside from the spaghetti code). It'll warn you if data is missing, if your basket counts go down versus up, etc etc.

If you're thinking about switching over the code so that it reads CSV files instead of TSV files, **DON'T DO THIS!**. A large majority of region names will have commas in them (State, Country is a good example), and CSV literally stands for Comma Separated Values. This is why the tool reads TSV (Tab Separated Values) files only.

(Yes, quotes are very much an option for CSV files! However, we're playing it safe, and using TSV files. Not all spreadsheet software may be smart enough to encase cells that have commas in them with quotes, so TSV is a consistent option as a result.)

Important information about licensing can be found at the bottom of this readme.

# A note about weather data
Right now, we're using the Dark Sky API which doesn't allow for new signups. If you don't have an API key for Dark Sky, the route compiler defaults the weather to 70 degrees F and clear. If you do have an API key, you can put your key in verycoolconvert.py, in the variable `ds_api_key`.

For continuity for the 2022 season, we are planning to implement the OpenWeatherAPI in the Route Compiler for Version 6. It's not a perfect substitute for Dark Sky, especially since Dark Sky was pay-as-you-go, but OpenWeatherMap is subscription based.

You can substitute your own API in, but you'll need a compatibility layer to translate the icon text from the API you're substituting in to that of the Dark Sky icon text set. You can also change the source code of the tracker to use a different iconography set. We use the weather-icons CSS library so the full guide to the API icon list is here: https://erikflowers.github.io/weather-icons/api-list.html

# Note about API calls with weather
The route works such that at compile time, the route compiler will get weather data when the Easter Bunny is expected to arrive at each stop. This means that every time you compile, you'll also be billed for the amount of tracking stops (pre-tracking stops get skipped) you have in your route. Make sure you have enough API capacity to compile your route.

# Creating your route
To create a route for the tracker, you'll first need to open up your preferred spreadsheet software (we use Google Sheets which works fine). You'll need to create your route in such a way that follows this template, as this is what the v5 compiler is reading.

| Data Row (new field) | Unix arrival time | Pretty arrival (GMT)                              | Pretty arrival (EDT)                              | City                                  | Region                                                  | Country Code                                                                                                                                                                 | Baskets Delivered | Carrots Eaten (new field) | Latitude                           | Longitude                            | Population as String                                                             | Population (new) | Population Year (new) | Elevation as String                                                             | Elevation in meters (new)                               | Timezone (new)                                                                               | Wikipedia Link                                                                                         | Wikipedia description                                                                                             |
|----------------------|-------------------|---------------------------------------------------|---------------------------------------------------|---------------------------------------|---------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|---------------------------|------------------------------------|--------------------------------------|----------------------------------------------------------------------------------|------------------|-----------------------|---------------------------------------------------------------------------------|---------------------------------------------------------|----------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Type: Integer        | Type: Integer     | Not used in the compiler, set it to what you want | Not used in the compiler, set it to what you want | Type: String                          | Type: String. Use "pt" to indicate pre-tracking update. | Type: String as ISO 3166-1 alpha-2 country code (except for UK regions). Leave blank for pre-tracking updates. Full list of country codes here: https://flagicons.lipis.dev/ | Type: Integer     | Type: Integer             | Type: Float from -89.999 to 89.999 | Type: Float from -179.999 to 179.999 | Not used in route compiler. Use to convert v4-style population data to v5-style. | Type: Integer    | Type: Integer         | Not used in route compiler. Use to convert v4-style elevation data to v5-style. | Type: Integer. Elevation is represented in METERS here. | Type: String. Important for the local arrival time feature. Must be recognized by moment.js. | Type: String, but it should be a URL pointing to the Wikipedia entry for a stop (or some other source) | Type: String. We have a new tool that (mostly) grabs from Wikipedia and filters data out.                         |
| 0                    | 1234567890        | 1/1/00 4:00 AM GMT                                | 1/1/00 12:00 AM EDT                               | Easter Island - Easter Bunny wakes up | pt                                                      |                                                                                                                                                                              | 0                 | 0                         | 0.000                              | 0.000                                |                                                                                  | 0                | 0                     |                                                                                 | 0                                                       | Pacific/Easter                                                                               | (leave blank for pre-tracking)                                                                         | (leave blank for pre-tracking)                                                                                    |
| 1                    | 1234567899        | 1/1/00 10:00 PM GMT                               | 1/1/00 6:00 PM EDT                                | London                                | England, United Kingdom                                 | gb-eng                                                                                                                                                                       | 1500000000        | 30000000                  | 50.0138                            | -0.9858                              |                                                                                  | 8908081          | 2018                  |                                                                                 | 11                                                      | Europe/London                                                                                | https://en.wikipedia.org/London                                                                        | London is a city. Make sure this description is at least a few sentences long, and try to keep things consistent. |


The route compiler starts reading the route at the second row. 

## Changes from v4 -> v5 route compiler
We've made some changes to the route file, including the release of two new tools to help you make a route more efficiently.
* Data Row column (everything shifts right by 1 column). Mostly for your sanity when investigating errors from the route compiler.
* Our baskets delivered column was generated by doing 4.28 * the population of a stop for each stop's count, then just summing everything up. We recommend you use the population of a stop to generate an accurate baskets delivered count.
* Carrots Eaten column - this is a new metric as part of the v5 update. We recommend you make this ~125x lower than your baskets delivered count.
* Population/Population Year - Used to improve the accuracy of the baskets delivered count. We kept the population-as-a-string representation from v4 to help from migration, although this will go away in v6 route formats. Population Year is used in the extended stop data window. Sometimes you'll find cities that have a population but not a year, set this to 0 to hide it from the stop informaiton window.
* Elevation in meters - Used as part of improving localization for the tracker. It's very important you input the data here as meters.
* Timezone - Used for the local arrival time feature. This needs to be in a format like "America/New_York". Google Maps has an API that allows you to grab the timezone of a stop in this representation, and a tool will be released to help you do this.
* Wikipedia description - We'll be releasing a tool soon to help you generate more consistent wikipedia descriptions by grabbing from wikipedia.

After that, go to town on creating your route. Pre-tracking starts first, we highly recommend that you have pre-tracking (the tracker is untested without pre-tracking). You cannot enter pre-tracking later, and there is no post-tracking. You should note that there are a few caveats.

We have included our route TSV file (as route-2021.tsv) for some reference, in case you need it.

Once your route is done, export it as a .tsv, and rename it to route.tsv. Copy & paste the file into the folder with the route compiler, and run verycoolconvert.py! The compile process should take about 1 minute per 100-250 stops, depending on your internet connection.

Once the compilation is done, take the route.json output file, and put it into /assets/routes/ in the tracker. Make sure you adjust the tracking start/pre tracking start timestamps in index.html & countdown/index.html so that the tracker knows when to start reading the route, and so that proper redirections can be made!

We also included the route.json output for the 2021 tracking run (route-2021.json), if you'd like to compare your compiled route to ours.

# Caveats (and important things to know)
When putting in the Wikipedia descriptions, you don't need to remove the references or notes. The route compiler automatically takes them out for you. Make sure you put a good amount of description in so that the thing isn't barebones.

The UNIX timestamp is the crux of the tracker - make sure this is right. We don't read the pretty arrival fields, and we mainly added them for internal use.

The tracker formats the full city name as (City), (Region). So, for New York City, the City name is `New York City`, and the Region is `New York, United States of America`. The fully formatted name is `New York City, New York, United States of America`.

The baskets delivered field needs to be an integer. If you're having troubles getting it to be without decimals, use the decrease decimals button in Google Sheets (and other software). We can say for sure that in Google Sheets, even if the actual raw value is a decimal, what gets exported is what is displayed to you.

In the tracker's environment variables, you'll need to set the name of the city where the tracker starts and ends. If you don't do this, a basket will be placed at this location and will obstruct the EB icon when the journey is complete. For our purposes, we'd set this to "Easter Bunny's Workshop".

We've only tested the tracker up to 600 stops totalling about 500 KB in route size. Obviously, as you increase the stop counter, so will your route size. Be mindful of this. The tracker should be able to accept a 10,000 stop route but it will have performance issues on slower machines - keep this in mind.

We generally recommend having at minimum 60 seconds between each stop. We kept our stop intervals at exact minutes (60 seconds, 120 seconds, etc), and we don't know how the tracker will react with not intervals that are a bit odd (say 37 seconds, 68 seconds, etc).

Having more even arrival intervals will also make it easier to put in the Unix timestamps for arrivals. If you have a set of stops with the same interval, you can fill out the unix arrival time for 2 stops, select those two stops, and drag down from the right bottom of the cell on the bottom of the 2 cells, and it'll fill in the unix timestamps according to that static interval.

The first pre-tracking should not start directly on the hour (say, at 2:00 AM EDT for pre-tracking during Easter 2021). Instead, have the first stop be about 5-10 seconds before when the tracker makes the transition from countdown to pre-tracking (1:59:55 AM EDT is what we used for pre-tracking first update during Easter 2021).

Beginning with v5, we automated getting Wikipedia descriptions. As mentioned before, a tool is coming out that will help you with this process. We still get population/elevation data manually, as there's no reliable way to scrape Wikipedia to get the most recent information.

Sometimes elevation cannot be found via Wikipedia, so a quick Google search should do the trick. In cases where an elevation range was provided on Wikipedia, we used the middle value of the range as the elevation.

# Exporting the route, and importing it into the route compiler
Once you're done creating your route, you'll want to export it as a .tsv file, and name it route (full file name should be route.tsv). Copy & paste the route.tsv file into where the route compiler is stored.

# Configuring the compiler
The compiler has an offset option in the code which allows you to compile your route with an offset to the unix timestamps in the route. This is useful when testing your tracker runs. You may have your actual run starting on a certain date, but then the test run can be offset by a certain number of seconds.

To calculate the offset, get the UNIX epoch time of when you want your run to start. Then, subtract this by the timestamp of the first stop. Your offset has been calculated.

# Running the compiler
Once all is set to go, the compiler will get to work getting all the data. If there are any issues with the data, the route compiler will output such in the terminal. The progress is also outputted as well, so you know where to look if you get a warning during the compile process.

Once this is done, upload route.json so that the tracker can use it (by default, it's under /assets/routes/). And you're good to go!

# Licensing
The route compiler, like the rest of this tracker, is open-sourced under GNU AGPL v3.0. 99% of the time, even if you modify the code, you won't need to distribute your modified code. This is a tool that is intended to be downloaded to your computer, and stay on your computer.

However, if you decide to modify the route compiler to the point where you're distributing it in an app, or via a website online, then you have to release your modified source code, and indicate changes that were made.

As mentioned in the main repo, we make no selling exceptions. Everyone has to abide by the GNU AGPL v3.0.

# Issues?
Please make an issue post.

# Contributing
We welcome contributions to improve the route compiler. There are many improvements that can be made to it!
