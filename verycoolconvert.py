#   track.easterbunny.cc Route Compiler v2.0.0
#   Copyright (C) 2021  track.easterbunny.cc
#
#   The track.easterbunny.cc Route Compiler is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   The track.easterbunny.cc Route Compiler is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with the track.easterbunny.cc Route Compiler.  If not, see <http://www.gnu.org/licenses/>.
#
#   Source code is available at https://gitlab.com/track-easterbunny-cc/route-compiler
#   License is available in the LICENSE file, or at https://www.gnu.org/licenses/agpl-3.0-standalone.html

import csv
import requests
import json
from vincenty import vincenty
import sys

offset = 0

route = {"destinations": {}}

ds_api_key = ""
dryrun = False
length = 0
distance_travelled_km = 0
distance_travelled_mi = 0
errors = 0
basestop_cityname = "Easter Bunny's Workshop"

with open("route.tsv") as fl:
    length = sum(1 for line in fl)

with open("route.tsv") as fc:
    rd = csv.reader(fc, delimiter="\t", quotechar='"')
    index = 0
    tempcont = 0
    prev_baskets = 0
    prev_carrots = 0

    for row in rd:
        if tempcont == 0:
            tempcont = 1
            continue

        if row[4] == "":
            print("City missing for row %s!" % str(index))
            errors = errors + 1

        if row[5] == "":
            print("Region missing for row %s!" % str(index))
            errors = errors + 1

        if row[1] == "":
            print("Unix arrival missing for row %s!" % str(index))
            errors = errors + 1

        if row[6] == "" and row[5] != "pt":
            print("Country Code missing for row %s!" % str(index))
            errors = errors + 1

        if row[7] == "":
            print("Baskets delivered missing for row %s!" % str(index))
            errors = errors + 1

        if row[8] == "":
            print("Carrots eaten missing for row %s!" % str(index))
            errors = errors + 1

        if row[9] == "":
            print("Latitude missing for row %s!" % str(index))
            errors = errors + 1

        if row[10] == "":
            print("Longitude missing for row %s!" % str(index))
            errors = errors + 1

        # TODO - remove PT limiter
        # TODO - Add checking for non-int number
        if row[12] == "" and row[5] != "pt" and row[4] != basestop_cityname:
            print("Population number missing for row %s!" % str(index))
            errors = errors + 1

        if row[13] == "" and row[5] != "pt" and row[4] != basestop_cityname:
            print("Population year missing for row %s!" % str(index))

        # TODO - remove PT limiter
        # TODO - Add checking for non-int number
        if row[15] == "" and row[5] != "pt" and row[4] != basestop_cityname:
            print("Elevation missing for row %s!" % str(index))
            errors = errors + 1

        if row[16] == "" and row[4] != basestop_cityname:
            print("Timezone missing for row %s!" % str(index))
            errors = errors + 1

        if row[17] == "" and row[5] != "pt" and row[4] != basestop_cityname:
            print("Link missing for row %s!" % str(index))
            errors = errors + 1

        if row[18] == "" and row[5] != "pt" and row[4] != basestop_cityname:
            print("Wikipedia description missing for row %s!" % str(index))
            errors = errors + 1

        if prev_baskets > int(row[7]):
            print("Basket count has gone down for row %s!" % str(index))
            errors = errors + 1

        if prev_carrots > int(row[8]):
            print("Carrots eaten count has gone down for row %s!" % str(index))
            errors = errors + 1

        prev_baskets = int(row[7])
        prev_carrots = int(row[8])
        index = index + 1

if errors > 0:
    if errors == 1:
        print("1 error has been found in the route data.")
    else:
        print(str(errors) + " errors have been found in the route data.")
    print("Would you like to continue with compilation? (Y)es/(N)o")
    confirm_comp = input("Input here: ")
    if confirm_comp.lower() != "yes" or confirm_comp.lower() != "y":
        if confirm_comp.lower() == "no" or confirm_comp.lower() == "n":
            print("Please fix the errors in the route data noted before compiling again!")
            sys.exit()
        else:
            print("Your input was not understood, answer defaulting to no.")
            print("Please fix the errors in the route data noted before compiling again!")
            sys.exit()
else:
    print("0 errors have been found in the route data, continuing with compilation!")

with open("route.tsv") as fd:
    rd = csv.reader(fd, delimiter="\t", quotechar='"')
    index = 0
    tempcont = 0
    prev_baskets = 0
    prev_carrots = 0


    for row in rd:
        if tempcont == 0:
            tempcont = 1
            continue
        # Check missing data
        print("Progress: %s/%s" % (str(index + 1), length - 1))
        # if row[4] == "":
        #     print("City missing for this row!")
        #
        # if row[5] == "":
        #     print("Region missing for this row!")
        #
        # if row[1] == "":
        #     print("Unix arrival missing for this row!")
        #
        # if row[6] == "" and row[5] != "pt":
        #     print("Country Code missing for this row!")
        #
        # if row[7] == "":
        #     print("Baskets delivered missing for this row!")
        #
        # if row[8] == "":
        #     print("Carrots eaten missing for this row!")
        #
        # if row[9] == "":
        #     print("Latitude missing for this row!")
        #
        # if row[10] == "":
        #     print("Longitude missing for this row!")
        #
        # if row[11] == "" and row[5] != "pt":
        #     print("Population missing for this row!")
        #
        # if row[12] == "" and row[5] != "pt":
        #     print("Elevation missing for this row!")
        #
        # if row[13] == "" and row[5] != "pt":
        #     print("Link missing for this row!")
        #
        # if row[14] == "" and row[5] != "pt":
        #     print("Description missing for this row!")
        #
        # if prev_baskets > int(row[7]):
        #     print("Basket count has gone down for this row!")
        #
        # if prev_carrots > int(row[8]):
        #     print("Carrots eaten count has gone down for this row!")

        prev_baskets = int(row[7])
        prev_carrots = int(row[8])
        route["destinations"][str(index)] = {}
        route["destinations"][str(index)] = {"weather": {}}
        route["destinations"][str(index)]["unixarrival"] = int(row[1]) - offset
        route["destinations"][str(index)]["city"] = row[4]
        route["destinations"][str(index)]["region"] = row[5]
        route["destinations"][str(index)]["countrycode"] = row[6]
        route["destinations"][str(index)]["eggsdelivered"] = int(row[7])
        route["destinations"][str(index)]["carrotseaten"] = int(row[8])
        route["destinations"][str(index)]["lat"] = float(row[9])
        route["destinations"][str(index)]["lng"] = float(row[10])
        route["destinations"][str(index)]["population"] = int(row[12])
        route["destinations"][str(index)]["population_year"] = str(row[13])
        route["destinations"][str(index)]["elevation"] = int(row[15])
        route["destinations"][str(index)]["timezone"] = row[16]
        link = row[17]
        link = link.replace("#Climate", "")
        route["destinations"][str(index)]["srclink"] = link

        descr = row[18]
        replacelist = [" (listen)", " [note 1]", " [note 2]", " [note 3]", " [note 4]",
                       " [note 5]", " [note 6]", " [note 7]", " [note 8]", " [note 9]",
                       " [note 10]", "[note 1]", "[note 2]", "[note 3]", "[note 4]",
                       "[note 5]", "[note 6]", "[note 7]", "[note 8]", "[note 9]", "[note 10]",
                       "[Note 1]", "[Note 2]", "[Note 3]", "[1]", "[2]", "[3]", "[4]", "[5]",
                       "[6]", "[7]", "[8]", "[9]", "[10]", "[11]", "[12]", "[13]", "[14]",
                       "[15]", "[16]", "[17]", "[18]", "[19]", "[20]", "[21]", "[22]", "[23]",
                       "[24]", "[25]", "[26]", "[27]", "[28]", "[29]", "[30]", "[a]", "[b]",
                       "[c]", "[d]", "[e]", "[f]", "[g]", "[h]", "[i]", "[j]", "[k]", "[l]",
                       "[m]", "[n]", "[o]", "[p]", "[q]", "[r]", "[s]", "[t]", "[u]", "[v]",
                       "[w]", "[x]", "[y]", "[z]", "[citation needed]"]

        for item in replacelist:
            descr = descr.replace(item, "")

        descr = descr.replace("km2", "km²")
        descr = descr.replace("km^2", "km²")
        if descr.endswith(".") is False:
            descr = descr + "."
        descr = descr.replace(". .", ".")
        descr = descr.replace(" , ", ", ")
        descr = descr.replace("  ", " ")
        descr = descr.replace(" .", ".")
        if descr == ".":
            descr = ""
        route["destinations"][str(index)]["descr"] = descr
        # This prevents weather data being fetched for PT.
        if route["destinations"][str(index)]["region"] != "pt":
            if dryrun is False:
                if row[4] == "International Space Station":
                    tempF = -250
                    tempC = (tempF - 32) * (5/9)
                    summary = "Very Cold"
                    icon = "clear-night"
                else:
                    weatherdata = requests.get("https://api.darksky.net/forecast/%s/%s,%s,%s?exclude=minutely,hourly,daily" % (ds_api_key, float(row[9]), float(row[10]), (int(row[1]) - offset)))
                    weatherdata_json = weatherdata.json()
                    try:
                        tempF = weatherdata_json["currently"]["temperature"]
                    except KeyError:
                        tempF = 70
                        print("Temperature data unavailable for this stop!")

                    tempC = (tempF - 32) * (5/9)
                    try:
                        summary = weatherdata_json["currently"]["summary"]
                    except KeyError:
                        summary = "Clear"
                        print("Summary data unavailable for this stop!")

                    try:
                        icon = weatherdata_json["currently"]["icon"]
                    except KeyError:
                        icon = "clear-night"
                        print("Icon data unavailable for this stop!")

                    if summary == "Humid" and icon == "clear-night":
                        summary = "Humid and Clear"
                    elif summary == "Humid" and icon == "clear-day":
                        summary = "Humid and Sunny"
            else:
                if row[4] == "International Space Station":
                    tempF = -250
                    tempC = (tempF - 32) * (5 / 9)
                    summary = "Very Cold"
                    icon = "clear-night"
                else:
                    tempF = 70
                    tempC = 20
                    summary = "Clear"
                    icon = "clear-night"

            summary = summary.replace("Possible ", "")
            route["destinations"][str(index)]["weather"]["tempC"] = int(round(tempC, 0))
            route["destinations"][str(index)]["weather"]["tempF"] = int(round(tempF, 0))
            route["destinations"][str(index)]["weather"]["summary"] = summary
            route["destinations"][str(index)]["weather"]["icon"] = icon
        index = index + 1

print("R1 over...starting R2!")

index = 0
for key in route['destinations'].keys():
    print("Progress: %s/%s" % (str(index + 1), length - 1))
    try:
        point1 = (float(route["destinations"][str(index - 1)]["lat"]), float(route["destinations"][str(index - 1)]["lng"]))
        point2 = (float(route["destinations"][str(index)]["lat"]), float(route["destinations"][str(index)]["lng"]))
        temp_travelled_km = vincenty(point1, point2)
        temp_travelled_mi = vincenty(point1, point2, miles=True)
        route["destinations"][str(index)]["distance-km"] = distance_travelled_km + temp_travelled_km
        distance_travelled_km += temp_travelled_km
        route["destinations"][str(index)]["distance-mi"] = distance_travelled_mi + temp_travelled_mi
        distance_travelled_mi += temp_travelled_mi
    except:
        print("Distance calculation failed.")
        temp_travelled_km = 0
        temp_travelled_mi = 0
        route["destinations"][str(index)]["distance-km"] = distance_travelled_km
        route["destinations"][str(index)]["distance-mi"] = distance_travelled_mi

    try:
        point1 = (float(route["destinations"][str(index)]["lat"]), float(route["destinations"][str(index)]["lng"]))
        point2 = (float(route["destinations"][str(index + 1)]["lat"]), float(route["destinations"][str(index + 1)]["lng"]))
        speed_travelled_km = vincenty(point1, point2)
        speed_travelled_mi = vincenty(point1, point2, miles=True)
        delta = float(route["destinations"][str(index + 1)]["unixarrival"]) - float(route["destinations"][str(index)]["unixarrival"])
        route["destinations"][str(index)]["speed-kph"] = (speed_travelled_km / delta) * 3600
        route["destinations"][str(index)]["speed-mph"] = (speed_travelled_mi / delta) * 3600
    except:
        print("Speed calculation failed.")
        route["destinations"][str(index)]["speed-kph"] = 0
        route["destinations"][str(index)]["speed-mph"] = 0

    index = index + 1


with open('route.json', 'w') as fp:
    json.dump(route, fp)

print("---------")
print("If you see more than one Speed calculation failed, that means it actually failed.")
print("If you just see one, it didn't fail.")
print("---------")
print("Complete!")
